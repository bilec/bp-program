package cipher;

import dic.Dictionary;

public class ZSD {

    private Dictionary dictionary;

    public ZSD()
    {
        dictionary = new Dictionary();
    }

    // https://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java

    //sifrovat
    public String encrypt( String text )
    {
        StringBuilder stringBuilder = new StringBuilder();

        String[] words = text.split( " (?=.)" );

        for( String word : words )
        {
            stringBuilder.append( dictionary.findWordNumber( word ) + dictionary.findWordPage( word ) + " " );
        }

        return stringBuilder.toString();
    }

    //desifrovat
    public String decrypt( String text )
    {
        StringBuilder stringBuilder = new StringBuilder();

        String[] words = text.split( " " );
        String wordNumber, pageNumber;

        for( String word : words )
        {
            wordNumber = word.substring( 0, 2 );
            pageNumber = word.substring( 2 );
            stringBuilder.append( dictionary.findWord(pageNumber, wordNumber) + " " );
        }

        return stringBuilder.toString();
    }
}

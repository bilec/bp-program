package dic;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import java.io.*;

public class Page {

    private String pageNumber;

    //         <number, word  > < key, value>
    private BiMap<String, String> pageWords     = HashBiMap.create();
    private BiMap<String, String> repeatedWords = HashBiMap.create();


    public Page( String fileName )
    {
        File toRead = new File( "src/main/resources/dic/" + fileName + ".txt" );
        String number, word;
        String line, repeat;
        try{
            BufferedReader bufReader = new BufferedReader(new FileReader( toRead ));
            pageNumber = bufReader.readLine();
            bufReader.readLine();
            while( ( line = bufReader.readLine() ) != null )
            {
                number = line.substring( 0, 2 );
                word = line.substring( 3 );
                repeat = pageWords.put( number, removeSpaces( word ) );

                if( repeat != null )
                {
                    repeatedWords.put( number, removeSpaces( word ) );
                }
            }
        }
        catch( FileNotFoundException e )
        {
            System.out.println( e.getMessage() );
        }
        catch( IOException e )
        {
            System.out.println( e.getMessage() );
        }
        catch( Exception e )
        {
            System.out.println( e.getMessage() );
        }
    }

    public String getPageNumber()
    {
        return pageNumber;
    }

    public String findWord( String wordNumber )
    {
        String retWord = pageWords.get( wordNumber );

        if( retWord == null )
        {
            retWord = repeatedWords.get( wordNumber );
        }

        return  retWord;
    }

    public String findWordNumber( String word )
    {
        String retWordNumber = pageWords.inverse().get( word );

       if( retWordNumber == null )
       {
           retWordNumber = repeatedWords.inverse().get( word );
       }

        return retWordNumber;
    }

    public void printPage()
    {
        for (BiMap.Entry<String, String> entry : pageWords.entrySet()) {
            System.out.println( entry.getKey() + "-" + entry.getValue() );
        }

        System.out.println();

        for (BiMap.Entry<String, String> entry : repeatedWords.entrySet()) {
            System.out.println( entry.getKey() + "-" + entry.getValue() );
        }
    }

    private String removeSpaces( String word )
    {
        String tmp = word;

        while( tmp.endsWith( " " ) )
        {
            tmp = tmp.substring( 0 , tmp.length() - 1 );
        }

        return tmp;
    }

}

package dic;

import java.util.ArrayList;
import java.util.List;

public class Dictionary {

    private List<Page> pages = new ArrayList<>();

    public Dictionary()
    {

        //zvacsit cisla ako sa zvacsi slovnik
        for( int i = 7; i < 8; i++ )
        {
            String fileName = String.format( "%03d", i );
            Page page = new Page( fileName );
            pages.add( page );
        }

    }

    public String findWord( String pageNumber, String wordNumber )
    {
        String word = null;

        for( Page page : pages )
        {
            if( pageNumber.equals( page.getPageNumber() ) )
            {
                word = page.findWord( wordNumber );
            }
        }

        return word;
    }

    public String findWordNumber( String word )
    {
        String retWordNumber = null;

        for( Page page : pages )
        {
            String tmp;
            if(  ( tmp = page.findWordNumber( word ) ) != null )
            {
                retWordNumber = tmp;
            }
        }

        return retWordNumber;
    }

    public String findWordPage( String word )
    {
        String retWordPage = null;

        for( Page page : pages )
        {
            if( page.findWordNumber( word ) != null )
            {
                retWordPage = page.getPageNumber();
            }
        }

        return retWordPage;
    }

}

package common;

import java.util.ArrayList;
import java.util.List;

public class Helper
{

    public static List<String> getDicFileNames()
    {
        List<String> retList = new ArrayList<>();

        //tu zvacsit casom
        for( int i = 7; i < 8; i++ )
        {
            retList.add( String.format( "%03d", i ) );
        }

        return retList;
    }

    public static List<String> getTableFileNames()
    {
        List<String> retList = new ArrayList<>();

        retList.add( "5648" );
        retList.add( "5649" );

        for( int i = 0; i < 6; i++ )
        {
            retList.add( "" + ( 5664 + i ) );
        }

        return retList;
    }

    public static String[] getShowList()
    {
        List<String> retList = new ArrayList<>();

        List<String> dic = new ArrayList<>();
        List<String> table = new ArrayList<>();

        for( String i : getDicFileNames() )
        {
            i += " - slovník";
            dic.add( i );
        }

        for( String i : getTableFileNames() )
        {
            i += " - prešifrovacia tabulka";
            table.add( i );
        }

        retList.addAll( dic );
        retList.addAll( table );

        return retList.toArray(String[]::new);
    }

    public static String getDicPath( String dicName )
    {
        String retVal = "src/main/resources/dic/";

        retVal += dicName;
        retVal += ".txt";

        return retVal;
    }

    public static String getTablePath( String tableName )
    {
        String retVal = "src/main/resources/table/";

        retVal += tableName;
        retVal += ".txt";

        return retVal;
    }


}

package listeners;

import common.Helper;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class ComboBoxListener implements ActionListener
{
    private JTextArea jTextArea;

    public ComboBoxListener( JTextArea jTextArea )
    {
        this.jTextArea = jTextArea;
    }

    @Override
    public void actionPerformed( ActionEvent e )
    {
        JComboBox cb = (JComboBox)e.getSource();
        String fileName = (String)cb.getSelectedItem();

        String[] partOfName = fileName.split( " - " );

        File toRead;
        if( partOfName[1].equals( "slovník" ) )
        {
            toRead = new File( Helper.getDicPath( partOfName[0] ) );
        }
        else
        {
            toRead = new File( Helper.getTablePath( partOfName[0] ) );
        }

        try
        {
            String line, retString = "";
            BufferedReader bufReader = new BufferedReader(new FileReader( toRead ));
            while( ( line = bufReader.readLine() ) != null )
            {
                retString += line + "\n";
            }
            jTextArea.setText( retString );
        }
        catch ( FileNotFoundException ex )
        {
            System.out.println( ex.getMessage() );
        } catch ( IOException ex )
        {
            System.out.println( ex.getMessage() );
        }

    }
}

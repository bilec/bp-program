package listeners;

import cipher.ZSD;
import gui.MyFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonListener implements ActionListener {

    private MyFrame myFrame;
    private int cardNumber;
    private ZSD zsd;

    public ButtonListener( MyFrame myFrame, int cardNumber )
    {
        this.myFrame = myFrame;
        this.cardNumber = cardNumber;
        zsd = new ZSD();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if( e.getActionCommand().equals( "Vymaž všetko" ) )
        {
            if( cardNumber == 1 )
            {
                myFrame.getInputText1().setText( "" );
                myFrame.getOutputText1().setText( "" );
            }
            else if( cardNumber == 2 )
            {
                myFrame.getInputText2().setText( "" );
                myFrame.getOutputText2().setText( "" );
            }
        }
        else if( e.getActionCommand().equals( "Šifruj" ) )
        {
            String toDo = myFrame.getInputText1().getText();
            //zavola akciu ktora spravy co ma
            //vypise do vystupu text
            toDo = zsd.encrypt( toDo );
            myFrame.getOutputText1().setText( toDo );

        }
        else if( e.getActionCommand().equals( "Dešifruj" ) )
        {
            String toDo = myFrame.getInputText2().getText();
            //zavola akciu ktora spravy co ma
            //vypise do vystupu text
            toDo = zsd.decrypt( toDo );
            myFrame.getOutputText2().setText( toDo );
        }
    }
}

﻿package gui;

import listeners.ButtonListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class MyFrame extends JFrame {

    private JTabbedPane jtp;
    private JPanel card1, card2, card3;
    private JTextArea inputText1, outputText1;
    private JTextArea inputText2, outputText2;
    private JTextArea showText;

    public MyFrame()
    {
        super( "Šifrovací kód ZSD" );

        jtp = new JTabbedPane();
        card1 = getJPanel( 1 );
        card2 = getJPanel( 2 );
        card3 = new JPanel();
        card4 = new JPanel();
        card5 = getShow();

        add( jtp );

        jtp.addTab( "Šifrovanie", card1 );
        jtp.addTab( "Dešifrovanie", card2 );
        jtp.addTab( "Prešifrovanie", card3 );
        jtp.addTab( "Odšifrovanie", card4 );
        jtp.addTab( "Zobrazenie slovníka a tabuliek", card5 );

        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        setIconImage( loadIcon() );

        setSize( 600, 400 );
        setVisible(true);
    }

    private JPanel getJPanel( int cardNumber )
    {
        JPanel retPanel = new JPanel();
        retPanel.setLayout( new BoxLayout( retPanel, BoxLayout.Y_AXIS ) );

        Label inputLabel  = new Label( "Vstup: " );
        Label outputLabel = new Label( "Výstup:" );

        JButton clean1 = new JButton( "Vymaž všetko" );
        JButton clean2 = new JButton( "Vymaž všetko" );
        JButton encrypt = new JButton( "Šifruj" );
        JButton decrypt = new JButton( "Dešifruj" );

        clean1.addActionListener( new ButtonListener( this, 1 ) );
        clean2.addActionListener( new ButtonListener( this, 2 ) );
        encrypt.addActionListener( new ButtonListener( this, 1 ) );
        decrypt.addActionListener( new ButtonListener( this, 2 ) );

        JPanel smallPanel = new JPanel();
        smallPanel.setLayout( new BoxLayout( smallPanel, BoxLayout.X_AXIS ) );

        if( cardNumber == 1 )
        {
            inputText1  = new JTextArea();
            outputText1 = new JTextArea();

            smallPanel.add( encrypt );
            smallPanel.add( clean1 );
            retPanel.add( smallPanel );

            retPanel.add( inputLabel );
            retPanel.add( inputText1 );
            retPanel.add( outputLabel );
            retPanel.add( outputText1 );
        }
        else if( cardNumber == 2 )
        {
            inputText2  = new JTextArea();
            outputText2 = new JTextArea();

            smallPanel.add( decrypt );
            smallPanel.add( clean2 );
            retPanel.add( smallPanel );

            retPanel.add( inputLabel );
            retPanel.add( inputText2 );
            retPanel.add( outputLabel );
            retPanel.add( outputText2 );
        }

        return retPanel;
    }

    private JPanel getShow()
    {
        JPanel retPanel = new JPanel();
        retPanel.setLayout( new BoxLayout( retPanel, BoxLayout.Y_AXIS ) );

        showText = new JTextArea();
        JScrollPane jScrollPane = new JScrollPane( showText );

        JComboBox jComboBox = new JComboBox( Helper.getShowList() );
        jComboBox.addActionListener( new ComboBoxListener( showText ) );
        jComboBox.setMaximumSize( jComboBox.getPreferredSize() );

        retPanel.add( jComboBox );
        retPanel.add( jScrollPane );

        return retPanel;
    }

    private Image loadIcon()
    {
        try
        {
            File toImage = new File( "src/main/resources/icon/icon.png" );
            return ImageIO.read( toImage );
        }
        catch ( IOException e )
        {
            System.out.println( e.getMessage() );
        }
        return null;
    }

    public JTextArea getInputText1() {
        return inputText1;
    }

    public JTextArea getOutputText1() {
        return outputText1;
    }

    public JTextArea getInputText2() {
        return inputText2;
    }

    public JTextArea getOutputText2() {
        return outputText2;
    }
}
